package com.payroll.unitTest.businessLogic;

import com.payroll.businessLogic.HourlyEmployee;
import com.payroll.businessLogic.TimeCard;
import com.payroll.businessLogic.TimeUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HourlyEmployeeTest {
    @Test
    public void testMorningOnly() {
        // Arrange
        HourlyEmployee johnDoe = new HourlyEmployee("Bob", 120);
        johnDoe.addTimeCard(new TimeCard(TimeUtils.buildDate("2018/11/27 09:00:00"), TimeUtils.buildDate("2018/11/27 12:00:00")));

        // Act
        double salary = johnDoe.calculatePay();

        // Assert
        double expectedSalary = 360;
        assertEquals(expectedSalary, salary,0.001);
    }

    @Test
    public void testFullDayNoOTOnly() {
        // Arrange
        HourlyEmployee johnDoe = new HourlyEmployee("Bob", 120);
        johnDoe.addTimeCard(new TimeCard(TimeUtils.buildDate("2018/11/27 09:00:00"), TimeUtils.buildDate("2018/11/27 18:00:00")));

        // Act
        double salary = johnDoe.calculatePay();

        // Assert
        double expectedSalary = 960;
        assertEquals(expectedSalary, salary,0.001);
    }

    // @Test
    // public void testFullDayWithOT() {
    //     IllegalArgumentException exc = new IllegalArgumentException("name of employee can not be null");
    //     IllegalArgumentException c;
    //     try{
    //         HourlyEmployee abc = new HourlyEmployee();
    //     }
    //     catch(IllegalArgumentException e){
    //         c = e;
    //     }
    //     assertEquals(c,exc);

    // }

    @Test
    public void testWorkingOnHoliday() {
    }
}
